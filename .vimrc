" must have posix shell
set shell=/bin/bash

" these are required some odd reason
set nocompatible
filetype off

" required because Vundle
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" plugins
Plugin 'VundleVim/Vundle.vim'
Plugin 'flazz/vim-colorschemes'
Plugin 'valloric/youcompleteme'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-rhubarb'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-repeat'
Plugin 'airblade/vim-gitgutter'
Plugin 'svermeulen/vim-easyclip'
Plugin 'junegunn/fzf'
Plugin 'scrooloose/nerdtree'
"Plugin 'kana/vim-fakeclip'
Plugin 'itchyny/lightline.vim'
Plugin 'KeitaNakamura/neodark.vim'
Plugin 'sheerun/vim-polyglot'
"Plugin 'pangloss/vim-javascript'
Plugin 'ervandew/supertab'
Plugin 'omnisharp/omnisharp-vim'
Plugin 'jiangmiao/auto-pairs'
Plugin 'othree/html5.vim'
Plugin 'OmniCppComplete'
Plugin 'pythoncomplete'
Plugin 'yggdroot/indentline'
Plugin 'othree/csscomplete.vim'
Plugin 'scrooloose/syntastic'


" required because Vundle
call vundle#end()
filetype plugin indent on

" things that don't make my life a living hell lolz
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif
set laststatus=2
set mouse=a
set number
map <C-n> :NERDTreeFocus<CR>

syntax on
colorscheme onedark
