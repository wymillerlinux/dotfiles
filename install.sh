#!/bin/bash
# Execute this first unless you know what you are doing. Enjoy.
# This script assumes you have git already installed on your computer.
# Written by Wyatt J. Miller, 2018
# Licensed by the MIT

# Set terminal colors
wget https://raw.githubusercontent.com/denysdovhan/gnome-terminal-one/master/one-dark.sh && . one-dark.sh

# Download powerline fonts cause they're great, okay?
mkdir Source
cd Source/
git clone https://github.com/powerline/fonts
cd fonts/
bash install.sh
cd $HOME

# Download Vundle cause that's great too, right?
git clone https://github.com/VundleVim/Vundle.vim.git $HOME/.vim/bundle/Vundle.vim

# Set terminal font
gconftool-2 --set /apps/gnome-terminal/profiles/Default/use_system_font --type=boolean false
gconftool-2 --set /apps/gnome-terminal/profiles/Default/font --type string "SauceCodePro Nerd Font Mono Medium"

# Install all the things
sudo apt update
sudo apt install -y fish tmux vim
chsh -s /usr/bin/fish

# Copy files over
cd Source/dotfiles
cp .tmux.conf $HOME
cp .vimrc $HOME
cp config.fish $HOME/.config/fish/
cd $HOME 

# Wrap up
echo "Done! Logout of your DE or your TTY to have your all your things."
done
